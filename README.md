[![Plop Grizzly](https://plopgrizzly.com/images/logo-bar.png)](https://plopgrizzly.com)

# [The Cala Physics Engine](https://crates.io/crates/cala)
A physics engine written in Rust, uses
[ADI / Screen](https://crates.io/crates/adi_screen) for graphics.

## Features
* Simulate physics in realtime using ADI to display what's happening.

## Roadmap to 1.0 (Future Features)
* Lots of stuff (TODO: explain)

## Change Log
### 0.0.2
* Update adi_screen
* Add widget function for HUD functionality (WIP).

### 0.0.1
* Initial release
